const fastify = require("fastify")({
  logger: true,
});
const pino = require("pino");
const pinoHttp = require("pino-http");

// Create a transport
const transport = pino.transport({
  targets: [
    {
      target: "pino-pretty",
      options: {
        destination: "./logs/app.log",
        mkdir: true,
        colorize: false,
        translateTime: "SYS:dd-mm-yyyy HH:MM:ss",
      },
    },
    {
      target: "pino-pretty",
      options: { destination: process.stdout.fd },
    },
  ],
});

// Create a logger instansce
const logger = pino(
  {
    level: "info",
    redact: {
      paths: ["req.headers", "req.remoteAddress", "req.remotePort"],
      remove: true,
    },
  },
  transport
);

app.use(pinoHttp({ logger }));
fastify.get("/", (req, res) => {
  logger.info(req, "Request to server");
  res.send({ hello: "world" });
});

fastify.listen({ port: 3000 });
