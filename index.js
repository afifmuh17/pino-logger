const pino = require("pino");

const token = "qFp3vqkYe5ybnezYRfUyP5ZU";

// Create a transport
const transport = pino.transport({
  targets: [
    {
      target: "pino-pretty",
      colorize: false,
      options: { destination: "./logs/output.log", mkdir: true },
    },
    {
      target: "pino-pretty",
      options: { destination: process.stdout.fd },
    },
    {
      target: "@logtail/pino",
      options: { sourceToken: token },
    },
  ],
});

// Create a logger instansce
const logger = pino(
  {
    level: "info",
    redact: {
      paths: ["email", "password", "address"],
      remove: true,
    },
  },
  transport
);

const employee = {
  id: 1,
  name: "Jane Doe",
  age: 35,
  email: "jane@doe.com",
  password: "54321",
  address: {
    street: "Crash Lane",
    city: "New York",
  },
};

logger.info(employee, "Employee records created");
